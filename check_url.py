#!/usr/bin/python3

# Copyright
#  Maurin VIDAL (maurin.vidal@geoazur.unice.fr)
#  CNRS / OCA / Geoazur Lab.
VERSION = "1.0.0"
PROG = "check_url.py"

import argparse
import http.client
from ftplib import FTP
from json import loads
from sys import stderr
from datetime import datetime

#Check argument
parser = argparse.ArgumentParser(description='Functionality:\nCheck Rinex files that are not availlable to be download on data center.', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--Version', '-v', action='version', version=PROG+' v'+VERSION)
parser.add_argument('--Quiet', '-q', action='store_true', default=False,
                    help='Disable notifications.\nPrint only errors on stderr.')
parser.add_argument('Sites', metavar='Sitename or #network', type=str, nargs='+',
                    help='Sitenames can be in 4 or 9 characters.\nTo retrieve all stations from a network, use "%%" special key before network name like "%%RENAG".')
parser.add_argument('--Out', '-o', metavar='Unavaillable file list path', type=argparse.FileType('w', encoding='UTF-8'), default=None,
                    help='All unavaillable files will be stored into the specified file path.\nThe file will be overwritten.')
parser.add_argument('--Glass', '-g', metavar='GLASS-API URL', type=str, default=None,
                    help='specify the complete GLASS-API URL including port like http://gnssdata-fr.oca.eu:8080.\nBy default it will retrieve metadata from DGW GLASS-API.\nThe sitename used to retrieve files metadata is in 4 characters,\nto avoid troubles with duplicates sites and files, please do not use default DGW GLASS-API.')
parser.add_argument('--Start', '-st', metavar='Start date', type=str, default=None,
                    help='Specify the start date (YYYY-MM-DD) of files to check.')
parser.add_argument('--End', '-ed', metavar='End date', type=str, default=None,
                    help='Specify the start date (YYYY-MM-DD) of files to check.')
args = parser.parse_args()


#######################
# GLASS API CONNEXION #
#######################
#configure default DGW Glass-API paramaters
if args.Glass is None:
    glass_proto="https"
    glass_url="gnssdata-epos.oca.eu"
    glass_port="443"
#parse parameters from custom Glass-API URL
else:
    if "http://" == args.Glass[:7]:
        glass_proto="http"
        if ':' in args.Glass[8:]:
            glass_url,glass_port = args.Glass[7:].split(':')
        else:
            glass_url = args.Glass[7:]
            glass_port = "8080"
    elif "https://" == args.Glass[:8]:
        glass_proto="https"
        if ':' in args.Glass[8:]:
            glass_url,glass_port = args.Glass[8:].split(':')
        else:
            glass_url = args.Glass[8:]
            glass_port = "443"
    else:
        print('ERROR: GLASS-API URL not recognised: use http://server:port or https://server:port.', file=stderr, flush=True)
        exit()
#initiate Glass-API connection
if glass_proto == "https":
    api = http.client.HTTPSConnection(glass_url, glass_port)
else:
    api = http.client.HTTPConnection(glass_url, glass_port)

#################
# GET SITE LIST #
#################
sites = []
for sitename in args.Sites:
    # get network site list
    if '%' in sitename[0]:
        #define network request
        api.request('GET', '/GlassFramework/webresources/stations/v2/network/'+sitename[1:]+'/short/csv')
        #run request and print error exit if http status is not 200
        response = api.getresponse()
        if response.status != 200: 
            print('ERROR: Cannot retrieve {} network sites from {} API, ERROR{}: {}'.format(network, glass_url, response.status, response.reason), file=stderr, flush=True)
            exit()
        sites+=list(map( lambda x : x.decode('utf-8', 'replace').split(',')[1].lstrip() , response.readlines()[1:]))
        response.close()
    else:
       sites.append(sitename)
#sort and remove duplicates sites
sites=sorted(list(dict.fromkeys(sites)))
    
######################
# LOOP FOR ALL SITES #
######################
previous_server=None
for site in sites:
    #define files request
    api.connect()
    api.request('GET', '/GlassFramework/webresources/files/marker/'+site[:4]+'/json', headers={'accept': 'application/json'})
    #run request and print error exit if http status is not 200
    response = api.getresponse()
    if response.status != 200: 
        print('ERROR: Cannot retrieve {} files list from {} API, ERROR{}: {}'.format(site, glass_url, response.status, response.reason), file=stderr, flush=True)
        exit()
    files_dict=loads(response.read())
    api.close()
    ######################
    # LOOP FOR ALL FILES #
    ######################
    for file_dict in files_dict:
        #check date window
        if args.Start and datetime.strptime(args.Start, "%Y-%m-%d") > datetime.strptime(file_dict["reference_date"][:10], "%Y-%m-%d"):
            continue
        if args.End and datetime.strptime(args.End, "%Y-%m-%d") < datetime.strptime(file_dict["reference_date"][:10], "%Y-%m-%d"):
            continue
        url_server = file_dict["data_center"]["protocol"]+"://"+file_dict["data_center"]["hostname"]
        url_path = '/'+file_dict["data_center"]["data_center_structure"]["directory_naming"]+'/'+file_dict["relative_path"]+'/'+file_dict["name"]
        url_full = url_server+url_path
        #initiate data server connection
        #if not already done or server changed
        if not previous_server or previous_server != url_server:
            previous_server = url_server
            #HTTPS data connexion
            if file_dict["data_center"]["protocol"] == "https":
                web = http.client.HTTPSConnection(file_dict["data_center"]["hostname"])
            #HTTP data connexion
            elif file_dict["data_center"]["protocol"] == "http":
                web = http.client.HTTPConnection(file_dict["data_center"]["hostname"])
            #FTP data connexion
            elif file_dict["data_center"]["protocol"] == "ftp":
                ftp = FTP(file_dict["data_center"]["hostname"])
                ftp.login(user="anonymous", passwd="check_url.py@epos-eu.org")
        #check data is availlable
        #FTP
        if file_dict["data_center"]["protocol"] == "ftp":
            try:
                ftp.size(url_path)
                if not args.Quiet : print(url_full)
            except:
                if not args.Quiet : print("ERROR: "+url_full, flush=True)
                if args.Out : print(url_full, file=args.Out, flush=True)
        #WEB
        else:
            #define get file request
            web.request('HEAD', url_path)
            #check file url path exist
            response = web.getresponse()
            response.close()
            if response.status == 200:
                if not args.Quiet : print(url_full)
            else:
                if not args.Quiet : print("ERROR: "+url_full, flush=True)
                if args.Out : print(url_full, file=args.Out, flush=True)
    #close data server connection
    if previous_server:
        if file_dict["data_center"]["protocol"] == "ftp":
             ftp.close()
        else:
             web.close()
    previous_server = None
